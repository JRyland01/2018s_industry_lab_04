package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    private int numberOfCharacters;
    private char symbol;

    public Pattern(int numberOfCharacters, char symbol) {
        this.numberOfCharacters = numberOfCharacters;
        this.symbol = symbol;
    }

    public int getNumberOfCharacters() {
        return numberOfCharacters;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setNumberOfCharacters(int numberOfCharacters) {
        this.numberOfCharacters = numberOfCharacters;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
    public String toString() {
       String pattern =  "";
        for (int i=0;i<numberOfCharacters;i++){
            pattern = pattern + symbol;
        }
        return pattern;
}
}

